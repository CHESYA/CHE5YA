### Hi Everyone 👋


I am a developer based in Korea, my work is focusing on creating UI for dashboards and systems and making it easier for users to use them.
I hope my codes and npm packages I made will good for you.


### What I'm up to

Currently I'm focusing on the company's project I'm working for.
And i'm preparing to tidy up [vue-slim-stackedbar](https://www.npmjs.com/package/vue-slim-stackedbar).
This package has changed a lot compared to its original content as it was used for the company's project I'm working for.

### Click here to see the cute resume
[Click me! (lang: KO-KR)](https://www.notion.so/che5ya/CHESYA-UX-BX-4ddca2d3b2af4e9cb6d53f050bb7d505)


### Let's talk

You can message to me on [facebook](https://www.facebook.com/che5ya) or message to me on [twitch](https://www.twitch.tv/che5ya).
